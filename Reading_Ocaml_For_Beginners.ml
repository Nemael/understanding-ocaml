(* My questions:
Do I have to put two ; at the end of a function, and one ; at the end of a function call?
How to display nicely? (might be answered later in the book)
*)

let rec factorial a =
	if a = 1 then 1 else a * factorial (a-1);;
	
let rec factorial a =
	match a with
		1 -> 1
	|	_ -> a * factorial (a-1);; (* "_" matches anything *)
	
let isvowel c =
	match c with
		'a' | 'e' | 'i' | 'o' | 'u' -> true
	|	_ -> false;;

(* Peut utiliser 'a'..'z' pour tout les characteres de 'a' à 'z' (Fonctionne aussi pour les nombres *)

(* Making Lists

h Head is first element
t Tail is the rest of the elements

Operator :: ("cons") adds a single element tot he front of an existing list
Operator @ ("append") combine two lists together
*)

let isnil list =
	match list with
		[] -> true
	|	_ -> false;;

let rec length list =
	match list with
		[] -> 0
	|	h::t -> 1 + length t;;

let rec length list =
	match list with
		[] -> 0
	|	_::t -> 1 + length t;; (* Why can I replace h with _? What is h (head), and where is it defined? Why does it needs to be "_::t" (matched with itself) and not "_" matches with anything? *)

let rec length_inner l n =
	match l with
		[] -> n
	|	h::t -> length_inner t (n+1)

let length l = length_inner l 0;;

let rec odd_elements l =
	match l with
		[] -> []
	|	[a] -> [a]
	| a::_::t -> a :: odd_elements t;;

let rec take n l =
	if n = 0 then [] else
	match l with
		h::t -> h :: take (n-1) t

let rec drop n l =
	if n = 0 then l else
	match l with
		h::t -> drop (n-1) t;;



(* Insertion sort of a list *)
let rec insert x l =
	match l with
		[] -> [x]
	|	h::t ->
			if x <= h
				then x::h::t
				else h::insert x t

let rec sort l =
	match l with
		[] -> []
	|	h::t -> insert h (sort t)


let rec merge x y =
	match x, y with
		[], l -> l (* What is l? *)
	|	l, [] -> l
	|	hx::tx, hy::ty ->
		if hx < hy
			then hx::merge tx (hy::ty)
			else hy::merge (hx::tx) ty;;
			
let rec msort l =
	match l with
		[] -> []
	|	[x] -> [x]
	|	_ ->
			let left = take (length l / 2) l in (* length is a function, its result is divided by 2 *)
				let right = drop (length l / 2) l in
					merge (msort left) (msort right);;
					

factorial 5;
isvowel 'd';
