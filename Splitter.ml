(*
Functions used for manipulation of lists
Functions:
	length (list)
	take (n, list)
		Copy the n first elements of the list and returns them
	drop (n, list)
		Drop the n first elements of the list
	last (list)
	delete_last (list)
	append_item (list, item)
	print_int_list (list)
	print_2d_int_list (list)
	print_string_list (list)
	print_2d_string_list (list) 
*)

(*
Questions:
	-Why do the functions work with arguments in the order "n list", but not with arguments in the order "list n"? Is the order of arguments relevant to their type?
	-Help for the warning "this expression should have type unit." on function print_2d_list
	-Do they use an IDE? Which IDE should I use?  Is Camelia recommended?
	-How to do generic functions (for functions that prints an int or a string)
	-I don't understand how the return value of a function works. Is it the last manipulated value? Does Ocaml guess from type what the return value should be?
	-Put manager for errors. For example, when n < 0, it should error
	-Questions about types. Should the data be a list of list of char, a list of string? Is 0x80 the same size as a 0?
	-Page 102 out of 165, the line "just to make the type agree"? What does it mean?
	-In the print list functions, put a test if length list > 0 to avoid printing the last comma
	-Where should I push my code?
*)

(*
TODO:
	-Find out how to add a 0x80 bytes to a String - should be a bytes
*)

let rec length list =
	match list with
		[] -> 0
	|	h::t -> 1 + length t

let rec take n list =
	match list with
		[] -> []
	|	h::t ->
			if n < 0 then raise (Invalid_argument "n cannot be negative") else
				if n = 0 then [] else h::take (n - 1) t

let rec drop n list =
	match list with
		[] -> []
	|	h::t ->
			if n < 0 then raise (Invalid_argument "n cannot be negative") else
				if n = 0 then list else drop (n - 1) t

let rec first list =
	match list with
		[] -> raise Not_found
	|	h::t -> h

let rec last list =
	match list with
		[] -> raise Not_found
	|	[x] -> x
	|	_::t -> last t

let rec delete_last list =
	match list with
		[x] -> []
	|	[] -> []
	|	h::t -> h::delete_last t

let append_item list item =
	list @ [item]

let rec print_int_list list =
	match list with
		[] -> []
	|	h::t -> print_int h; if t != [] then print_string ","; print_int_list t

let rec print_2d_int_list list =
	match list with
		[] -> []
	|	h::t -> print_string "["; print_int_list h; print_string "]"; print_2d_int_list t

let rec print_string_list list =
	match list with
		[] -> []
	|	h::t -> print_string h; if t != [] then print_string ","; print_string_list t

let rec print_2d_string_list list =
	match list with
		[] -> []
	|	h::t -> print_string "["; print_string_list h; print_string "]"; print_2d_string_list t

let rec find_elem_index elem list index =
	match list with
		[] -> raise(Failure "Not Found")
	|	h::t -> if (h = elem) then index else find_elem_index elem t index+1 

(*
Splits data into multiple blocks

Functions:
	split_list (list, n)
	join_list (list)
		-Should verify that every piece of data is the same size
	split_string (string, n)
	join_string (string)
*)

let rec split_list list n = (* Takes a list and splits it in multiple nested lists of n length *)
	match list with
		[] -> []
	|	h::t -> take n list :: split_list (drop n list) n

let rec split_string string n = (* Takes a string and splits it in a list of strings of n length *)
	if n < String.length string
	then String.sub string 0 n :: split_string (String.sub string n ((String.length string) - n)) n
	else [String.sub string 0 (String.length string)]

let rec join_list list =
	List.concat list
	(*match list with
		[] -> []
	|	h::t -> h :: (join_list t) *)

let rec join_string_list list =
	[String.concat "" list]



(*
Pads the last block of data

Functions:
	pad (input, expected_size)
		Receive the last block of data and expected_size. Pads that block -if needed- for it to be the size of expected_size. Puts 0x80 followed by 0x00
	unpad (input, expected_size)
		Receive the last block of data and removes padding, if there is any. Checks for the byte 0x80 followed by only 0x00s, and removes every byte from 0x80

Questions:
	Could there be 0x80 in every data, even unpadded ones?
	What if the data is one byte short from being the block_size, should we pad 0x80 and nothing else? Because this 0x80 could be relevant data, because it is not followed by any 0x00. Or is 0x80 special? 0x80 is the termination character
*)


let rec pad_list list expected_size =
	let padding = Array.make (expected_size - (length list)) "0" in
	padding.(0) <- "0x80";
	list @ (Array.to_list(padding))

let pad list expected_size = 
	(delete_last list) @ [(pad_list (last list) expected_size)]

	
(*
let rec pad_string string expected_size =
	let padding = String.make (expected_size - (String.length string) -1) '0' in
	[(String.concat "" (string::["0x80"; padding]))]
*)
	
let rec unpad_list list =
	match list with
		"0x80"::t -> []
	|	h::t -> h::unpad_list t

let unpad list =
	(delete_last list) @ [(unpad_list (last list))]

(*

*)

let rec find_terminator_index list =
	find_elem_index "0x80" list 0

;;

let splitted_list = split_list ["a";"b";"c";"d";"e"] 4 in
print_string "Splitted list: \n";
print_2d_string_list splitted_list;
print_newline();

print_string "Padded list:\n";
let padded_list = pad splitted_list 4 in
print_2d_string_list padded_list;
print_newline ();

print_string "Terminator index: ";
print_int (find_terminator_index (last padded_list));
print_newline();

print_string "Unpadded list:\n";
let unpadded_list = unpad padded_list in
print_2d_string_list unpadded_list;
print_newline();

(*
print_string "Joined list:\n";
print_string_list (join_list splitted_list);
print_newline ();
*)
(*
print_string_list (last result_list);
*)

(*
let splitted_string = split_string "ABCDEFABCDEF" 7 in
print_string "Splitted string:\n";
print_string_list splitted_string;
print_newline ();
let padded_string = pad_string (last splitted_string) 7 in
let result_string = (delete_last splitted_string) @ padded_string in
print_string "Padded list:\n";
print_string_list padded_string;
print_newline ();
print_string "Joined string:\n";
print_string_list (join_string_list result_string);
print_newline ();
*)

(*
To do
	-Do the unpad function for list and string
	-Try to use a list of way higher size
	-Verify that it is 0x80 at the first element of the padding
	-Check what happens when there is no need for padding, the block is already correctly-sized
	-Function to remove the padding, use the same strategy as the function to put the padding 
*)

(*print_string Sys.argv.(1)
let message :bytes = Bytes.of_string "THFEURAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUraTfeaveHFEUAHGFOHREAGHORAHGOUHRAOUHGHRAVNRAOCNEAONVIOFANVRANUVPIRANVUra";;*)
